<?php
$startTime = microtime(true);

$pdo = new PDO('mysql:host=mysql-container;dbname=TestTree', 'Treelogin', 'Treeparol');

// Получение всех категорий
$stmt = $pdo->query('SELECT * FROM categories');
$categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Функция для построения дерева
function buildTree($elements, $parentId = 0) {
    $branch = [];

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['categories_id']);

            if ($children) {
                $branch[$element['categories_id']] = $children;
            } else {
                $branch[$element['categories_id']] = $element['categories_id'];
            }
        }
    }

    return $branch;
}

$tree = buildTree($categories);

$endTime = microtime(true);

$executionTime = $endTime - $startTime;

$treeWithExecutionTime = [
    'executionTime' => $executionTime,
    'tree' => $tree
];

$jsonTree = json_encode($treeWithExecutionTime, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

$jsonFilePath = '/app/tree.json';

file_put_contents($jsonFilePath, $jsonTree);

echo "Дерево категорий и время выполнения сохранены в файле 'tree.json'.\n";