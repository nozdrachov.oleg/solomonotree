FROM php:7.4-cli
RUN apt-get update && apt-get install -y --no-install-recommends libzip-dev zlib1g-dev libpq-dev zip  \
    && docker-php-ext-install zip mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/bin --filename=composer --quiet
RUN pecl install redis \
    && docker-php-ext-enable redis

ENV COMPOSER_ALLOW_SUPERUSER 1

WORKDIR /app
